.. _resolutions:

=============
 Resolutions
=============

When a motion does not result in a change in a reference doc, it can
be expressed as a resolution.

2019
====

.. toctree::
   :maxdepth: 1
   :glob:
   :reversed:

   2019*
