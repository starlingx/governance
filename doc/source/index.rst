====================
StarlingX Governance
====================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

StarlingX is a top-level Open Infrastructure Foundation confirmed project that
is governed by two separate bodies.

Board of Directors
==================

The `Open Infrastructure Foundation Board of Directors`_ has oversight over the
Open Infrastructure Foundation, and the assets the Foundation protects (such as
the OpenStack trademark). It is composed of appointed representatives from
Platinum sponsors, elected representatives from Gold sponsors, and elected
independent representatives.

.. _`Open Infrastructure Foundation Board of Directors`: https://openinfra.dev/about/board/


Technical Steering Committee
============================

The `StarlingX Technical Steering Committee`_ has oversight over StarlingX
technical matters such as project architectural decisions and managing the
sub-project life-cycle. It defines the overall project architecture and
sets the overall project priorities in collaboration with the community.

.. _`StarlingX Technical Steering Committee`: reference/tsc/

.. toctree::
    :glob:
    :maxdepth: 1

    reference/tsc/*
    resolutions/index

Indices and Tables
==================

* :ref:`genindex`
* :ref:`search`
